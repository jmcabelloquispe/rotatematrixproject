# PROYECTO ROTAR MATRIZ #

Ejecutar el proyecto desde eclipse o copiar [aqu�](https://www.onlinegdb.com/online_java_compiler) el contenido de la clase Matrix.java, la clase se debe llamar Main para que compile correctamente.

El programa solicitara una cadena con un formato definido. Ejm: [ [1,2,3], [4,5, 6], [7,8,9] ]

![alt text](https://i.ibb.co/Ky5Gw72/Screenshot-1.png)

El programa imprime la matriz original y la matriz rotada 

![alt text](https://i.ibb.co/Kzj8yMs/Screenshot-2.png) 
