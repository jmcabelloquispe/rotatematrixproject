import java.util.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 */

/**
 * @author jose.cabello
 *
 */
public class Matrix {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		while (true) {
			@SuppressWarnings("resource")
			Scanner input = new Scanner(System.in);
		    System.out.println("Ingresar Matriz:");
		    String text = input.nextLine(); 
		    if(true) {
		    	try {
			    	int matrix[][] = create(text);
			    	if(matrix != null) {
			    		System.out.println("Matriz original"); 
			    		print(matrix);
						System.out.println("Matriz rotada en sentido anti-horario (90 grados)."); 
						print(rotate(matrix));
						System.out.println();
			    	}else {
			    		System.err.println("Error en el formato de la matriz."); 
			    		System.out.println();
			    	}
		    	}catch(Exception e) {
		    		System.err.println("Error en el formato de la matriz.");
		    		System.out.println();
		    	}
		    }
		}
	}
	
	// function to print the matrix 
    static void print(int matrix[][]) { 
    	System.out.println(Arrays.deepToString(matrix)); 
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) 
                System.out.print( matrix[i][j] + " "); 
            System.out.println(); 
        } 
    }
	
    // function to rotate the matrix 
	static int[][] rotate(int[][] matrix) {
		int size = matrix.length;
		int[][] newMatrix = new int[size][size];
	    for(int i = 0, j = size - 1; i < size && j >= 0; i++, j--)
	        for(int k = 0; k < size; k++) 
	        	newMatrix[i][k] = matrix[k][j];           
	    return newMatrix; 
	}
	
	// function to create the matrix from string
	static int[][] create(String text) {
		Pattern pattern = Pattern.compile("^[\\[\\[][0-9\\[\\], ]*[\\]\\]]$");
	    Matcher matcher = pattern.matcher(text);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	    	String [] array = text.replaceAll("[\\[ ]+", "").split("],");
			int [][] matrix = new int[array.length][];
		    for(int i = 0; i < array.length; i++){
		        String [] row = array[i].split("\\D+");
		        matrix[i] = new int[row.length];
		        if(array.length == row.length) {
		        	for(int j = 0; j < row.length; j++){
			        	matrix[i][j] = Integer.valueOf(row[j]);
			        }  
		        }else {
		        	return null;
		        }    
		    }
		    return matrix;
	    } else {
	      return null;
	    }
	}
	
}
